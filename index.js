const checkbox = document.querySelector('.checkbox')
const priceBasic = document.querySelector('.basic .price')
const pricePro = document.querySelector('.professional .price')
const priceMaster = document.querySelector('.master .price')

checkbox.addEventListener('click', () => {
    if (checkbox.checked) {
        priceBasic.innerText = "$19.99"
        pricePro.innerText = "$24.99"
        priceMaster.innerText = "$39.99"
    } else {
        priceBasic.innerText = "$199.99"
        pricePro.innerText = "$249.99"
        priceMaster.innerText = "$399.99"
    }
})
